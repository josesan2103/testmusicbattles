package com.example.testmusicbattles.ui.modules.mainactivity.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.testmusicbattles.R
import com.example.testmusicbattles.ui.modules.mainactivity.adapter.GalleryImageAdapter
import com.example.testmusicbattles.ui.modules.mainactivity.adapter.Image
import com.example.testmusicbattles.ui.modules.mainactivity.interfaces.GalleryImageClickListener

/**
 * A simple [Fragment] subclass.
 */
class GaleriaFragment : Fragment(), GalleryImageClickListener {

    // gallery column count
    private val SPAN_COUNT = 2

    private val imageList = ArrayList<Image>()
    lateinit var galleryAdapter: GalleryImageAdapter

    lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_galeria, container, false)

        recyclerView = view.findViewById(R.id.recyclerView)
        // init adapter
        galleryAdapter = GalleryImageAdapter(imageList)
        galleryAdapter.listener = this
        // init recyclerview
        recyclerView.layoutManager = GridLayoutManager(activity, SPAN_COUNT)
        recyclerView.adapter = galleryAdapter
        // load images
        loadImages()

        return view;
    }

    private fun loadImages() {
        imageList.add(Image("https://raw.githubusercontent.com/bumptech/glide/master/static/glide_logo.png", "Glide ejemplo"))
        imageList.add(Image("http://images.bigcartel.com/product_images/166476442/15240_TOS_OKC_Fan_Main.png", "Otra imagen"))
        imageList.add(Image("http://images.bigcartel.com/product_images/166476442/15240_TOS_OKC_Fan_Main.png", "Otra imagen"))
        imageList.add(Image("https://raw.githubusercontent.com/bumptech/glide/master/static/glide_logo.png", "Glide ejemplo"))
        imageList.add(Image("https://raw.githubusercontent.com/bumptech/glide/master/static/glide_logo.png", "Glide ejemplo"))
        imageList.add(Image("http://images.bigcartel.com/product_images/166476442/15240_TOS_OKC_Fan_Main.png", "Otra imagen"))
        imageList.add(Image("http://images.bigcartel.com/product_images/166476442/15240_TOS_OKC_Fan_Main.png", "Otra imagen"))
        imageList.add(Image("https://raw.githubusercontent.com/bumptech/glide/master/static/glide_logo.png", "Glide ejemplo"))
        galleryAdapter.notifyDataSetChanged()
    }

    override fun onClick(position: Int) {

    }

}
