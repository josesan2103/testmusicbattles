package com.example.testmusicbattles.ui.modules.reproductormusica.fragment

import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.testmusicbattles.R

/**
 * A simple [Fragment] subclass.
 */
class MusicaFragment : Fragment(), View.OnClickListener {

    private lateinit var btnPlayMusic: Button
    private lateinit var btnStopMusic: Button
    private lateinit var btnAtrasar: Button
    private lateinit var btnAdelantar: Button

    private lateinit var mediaPlayer: MediaPlayer;
    val listSongs: IntArray = intArrayOf(R.raw.no_crescas_mas, R.raw.t_extranare, R.raw.demente, R.raw.no_esto_solo, R.raw.no_tengas_miedo)
    var index = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_musica, container, false)

        mediaPlayer = MediaPlayer.create(activity, listSongs[index])

        btnPlayMusic = view.findViewById(R.id.btnPlayMusic)
        btnStopMusic = view.findViewById(R.id.btnStopMusic)
        btnAtrasar = view.findViewById(R.id.btnAtrasar)
        btnAdelantar = view.findViewById(R.id.btnAdelantar)

        btnPlayMusic.setOnClickListener(this)
        btnStopMusic.setOnClickListener(this)
        btnAtrasar.setOnClickListener(this)
        btnAdelantar.setOnClickListener(this)

        return view
    }

    override fun onClick(view: View?) {
        val id = view!!.id
        when (id) {
            R.id.btnPlayMusic -> {
                if (mediaPlayer.isPlaying){
                    mediaPlayer.pause()
                    btnPlayMusic.background = resources.getDrawable(R.drawable.ic_play_arrow_white)
                } else {
                    mediaPlayer.start()
                    btnPlayMusic.background = resources.getDrawable(R.drawable.ic_pause_white)
                }
            }
            R.id.btnStopMusic -> {
                mediaPlayer.stop()
                mediaPlayer = MediaPlayer.create(activity, listSongs[index])
                btnPlayMusic.background = resources.getDrawable(R.drawable.ic_play_arrow_white)
            }
            R.id.btnAtrasar -> {
                index --
                if (index < 0) {
                    index = 4
                }
                mediaPlayer.stop()
                mediaPlayer = MediaPlayer.create(activity, listSongs[index])
                mediaPlayer.start()

            }
            R.id.btnAdelantar -> {
                index ++
                if (index > 4) {
                    index = 0
                }
                mediaPlayer.stop()
                mediaPlayer = MediaPlayer.create(activity, listSongs[index])
                mediaPlayer.start()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mediaPlayer.stop()
        mediaPlayer.release()
    }

}
