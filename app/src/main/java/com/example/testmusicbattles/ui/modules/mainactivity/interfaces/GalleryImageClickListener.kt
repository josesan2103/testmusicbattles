package com.example.testmusicbattles.ui.modules.mainactivity.interfaces

interface GalleryImageClickListener {

    fun onClick(position: Int)

}