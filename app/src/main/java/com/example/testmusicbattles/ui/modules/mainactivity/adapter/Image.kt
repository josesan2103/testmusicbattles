package com.example.testmusicbattles.ui.modules.mainactivity.adapter

data class Image (
    val imageUrl: String,
    val title: String
)