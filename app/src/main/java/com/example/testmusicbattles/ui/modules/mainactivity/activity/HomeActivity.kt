package com.example.testmusicbattles.ui.modules.mainactivity.activity

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.testmusicbattles.R
import com.example.testmusicbattles.ui.modules.mainactivity.fragment.GaleriaFragment
import com.example.testmusicbattles.ui.modules.perfilusuario.fragment.PerfilFragment
import com.example.testmusicbattles.ui.modules.reproductormusica.fragment.MusicaFragment
import com.google.android.material.navigation.NavigationView


class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var drawerLayout: DrawerLayout

    /**
     * toolbar
     */
    lateinit var toolbar: Toolbar

    lateinit var fragmentManager: FragmentManager
    lateinit var fragmentTransaction: FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        drawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener(this)

        fragmentManager = supportFragmentManager
        fragmentTransaction = fragmentManager.beginTransaction()
        val galeriaFragment = GaleriaFragment()
        fragmentTransaction.replace(R.id.nav_host_fragment, galeriaFragment)
        fragmentTransaction.commit()

    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.nav_galery -> {
                Toast.makeText(applicationContext, "Galeria", Toast.LENGTH_SHORT).show()
                setFragment(0);
                drawerLayout.closeDrawer(GravityCompat.START);
                return true
            }
            R.id.nav_music -> {
                Toast.makeText(applicationContext, "musica", Toast.LENGTH_SHORT).show()
                setFragment(1)
                drawerLayout.closeDrawer(GravityCompat.START)
                return true
            }
            R.id.nav_perfil -> {
                Toast.makeText(applicationContext, "perfil", Toast.LENGTH_SHORT).show()
                setFragment(2)
                drawerLayout.closeDrawer(GravityCompat.START)
                return true
            }
        }
        return true
    }

    fun setFragment(position: Int) {
        val fragmentManager: FragmentManager
        val fragmentTransaction: FragmentTransaction
        when (position) {
            0 -> {
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                val galeriaFragment = GaleriaFragment()
                fragmentTransaction.replace(R.id.nav_host_fragment, galeriaFragment)
                fragmentTransaction.commit()
            }
            1 -> {
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                val musicaFragment = MusicaFragment()
                fragmentTransaction.replace(R.id.nav_host_fragment, musicaFragment)
                fragmentTransaction.commit()
            }
            2 -> {
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                val perfilFragment = PerfilFragment()
                fragmentTransaction.replace(R.id.nav_host_fragment, perfilFragment)
                fragmentTransaction.commit()
            }
        }
    }

}
