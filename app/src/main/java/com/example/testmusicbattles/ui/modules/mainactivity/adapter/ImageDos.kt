package com.example.testmusicbattles.ui.modules.mainactivity.adapter

data class ImageDos(
    val imageUrl: Int,
    val title: String
)